import { GoogleMapsOverlay } from '@deck.gl/google-maps';
import { HexagonLayer } from '@deck.gl/aggregation-layers';
import { ScatterplotLayer } from '@deck.gl/layers';
import { GeoJsonLayer } from '@deck.gl/layers';
import { HeatmapLayer } from '@deck.gl/aggregation-layers';

/* Data source variables */
const nodesSourceData = 'table.json';
const segmSourceData = 'test.geojson';
const segmTrs = 'finance_2020_sgmt_trs_ready.geojson';
const segmRcd = 'finance_2020_sgmt_rcd_ready.geojson';

//

/* Scatterplot Layer */
const scatterplot = () => new ScatterplotLayer({
    id: 'scatter',
    data: nodesSourceData,
    opacity: 1,
    filled: true,
    radiusMinPixels: 6, 
    radiusMaxPixels: 7, 
    getPosition: d => [d.lng, d.lat],
    getFillColor: [255, 140, 0, 100], 
    pickable: true, 
    onHover: ({ object, x, y }) => {
        const el = document.getElementById('tooltip');
        if (object) {
            const { NID } = object;
            el.innerHTML = `<h1>ID ${NID}</h1>`
            el.style.display = 'block';
            el.style.opacity = 0.9;
            el.style.left = x + 'px';
            el.style.top = y + 'px'; 
            el.style.color = 'orange';
        } else {
            el.style.opacity = 0.0;
        } 
    },
 
    onClick: ({ object, x, y }) => {
        window.open(`https://www.netcity-bucuresti.ro`)
    }, 
});

/* Trs Layer */
const layerTrs = () =>
      new GeoJsonLayer({
        id: 'layerTrsGeoJson',
        data: segmTrs,
        opacity: 1,
        stroked: false,
        filled: true,
        lineWidthMinPixels: 2,
        lineWidthMaxPixels: 7,
        getLineColor: [1, 166, 226],
        pickable: true,
        parameters: {
          depthTest: false
        },
        onHover: info => setTooltip(info.object, info.x, info.y)
})

/** Tooltip for Trs Layer */
function setTooltip(object, x, y) {
    const el = document.getElementById('tooltip2');
    if (object) {
        el.innerHTML = `<h1>ID = ${object.properties.cartodb_id} </h1>`;
        el.style.display = 'block';
        el.style.left = x + 'px';
        el.style.top = y + 'px';
        el.style.color = 'red';
    } else {
        el.style.display = 'none';
    }
}

/* Rcd Layer */
const layerRcd = () =>
      new GeoJsonLayer({
        id: 'layerRcdGeoJson',
        data: segmRcd,
        opacity: 1,
        stroked: false,
        filled: true,
        lineWidthMinPixels: 1,
        lineWidthMaxPixels: 3,
        opacity: 1,
        getLineColor: [100, 266, 126],
        lineWidthMinPixels: 10,
        parameters: {
          depthTest: false
        }
    })    
    


/**Initiate MAP */
window.initMap = () => {
//Map load coordinates & Zoom Level
    const map = new google.maps.Map(document.getElementById('map'), {
        center: { lat: 44.424800, lng: 26.1180 },
        zoom: 13,
    });
//Google Maps Overlay -> 
    const overlay = new GoogleMapsOverlay({
        layers: [
        scatterplot(),
        layerRcd(),
        layerTrs()
        ],
        setTooltip,
    }); 
   overlay.setMap(map);

    var toggleableLayerIds = ['layerRcdGeoJson', 'layerTrsGeoJson', 'scatter'];

    for (var i = 0; i < toggleableLayerIds.length; i++) {
        var id = toggleableLayerIds[i];
        
        var link = document.createElement('a');
        link.href = '#';
        link.className = 'active';
        link.textContent = id;
        
        link.onclick = function(e) {
        var clickedLayer = this.textContent;
        e.preventDefault();
        e.stopPropagation();
        
        var visibility = map.getLayoutProperty(clickedLayer, 'visibility');
        
        if (visibility === 'visible') {
            map.setLayoutProperty(clickedLayer, 'visibility', 'none');
            this.className = '';
        } else {
            this.className = 'active';
            map.setLayoutProperty(clickedLayer, 'visibility', 'visible');
            }
        };
        
         var layers = document.getElementById('menu');
         layers.appendChild(link);
    }
      
} 
const csvToJson = require('convert-csv-to-json');
 
const input = './table.csv'; 
const output = './table.json';
 
csvToJson.fieldDelimiter(',')
         .formatValueByType()
         .generateJsonFileFromCsv(input, output);
